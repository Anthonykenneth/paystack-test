import React, { Component } from "react";

export default class Form extends Component {
  render() {
    return (
      <div>
        <form onSubmit={this.props.getUser}>
          <p>
            <input
              style={{ margin: "20px auto", display: "block" }}
              type="text"
              name="bvn"
            />
            <button>Submit</button>
          </p>
        </form>
      </div>
    );
  }
}
