import React, { Component } from "react";
import axios from "axios";
import logo from "./logo.svg";
import "./App.css";
import Form from "./components/Form";

//https://api.github.com/users/john           github api

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {}
    };

    this.getUser = this.getUser.bind(this);
  }

  getUser(e) {
    e.preventDefault();
    let axiosConfig = {
      headers: {
        Authorization: "Bearer sk_test_key" //Paystack secret test key for header
        //Note that there must be a "Bearer sk_test_randomNumbers" keyword
      }
    };
    const USERS_BVN = e.target.elements.bvn.value;
    //console.log(USERS_BVN);
    if (USERS_BVN) {
      //same as if(USERS_BVN === true)
      axios
        .get(
          `https://api.paystack.co/bank/resolve_bvn/${USERS_BVN}`,
          axiosConfig
        )

        .then(res => {
          console.log(res);

          const data = res.data.data;
          // const first_name = res.data.data.first_name;
          //console.log(first_name);
          this.setState({ data: data });
        })
        .catch(err => {
          alert("failed to fetch");
          console.log(err);
        });
    }
  }
  //.then is a result of whatever value we get from the api,  also, the .then() function takes in a (callback) function, and whatever argument being passed in the (callback) function is basically the result of the data that we got back from the api.

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Paystack Bvn Verification with React</p>
        </header>

        <Form getUser={this.getUser} />

        {this.state.data ? (
          <div>
            <p>First Name: {this.state.data.first_name}</p>
            <p>Last Name: {this.state.data.last_name}</p>
            <p>Date of Birth Name: {this.state.data.dob}</p>
            <p>Phone Number: {this.state.data.mobile}</p>
            <p>Bvn: {this.state.data.bvn}</p>
          </div>
        ) : (
          <p>Please enter Your BVN. </p>
        )}
      </div>
    );
  }
}

//note: we cannot use an if else statement directly, or any pure javascript inside jsx

//  : = else   ? = then

export default App;

//email to upload: damilare@pettycash.com.ng
